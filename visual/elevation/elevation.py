#!/usr/bin/env python

import matplotlib.pyplot as pyplot

def PlotElevation(distances, elevations, bounds, imgPath):
    pyplot.plot(distances, elevations, 'm-')
    pyplot.axis(bounds)
    pyplot.xlabel('Distance (km)')
    pyplot.ylabel('Elevation (m)')
    pyplot.savefig(imgPath)
