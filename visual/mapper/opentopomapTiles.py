#!/usr/bin/env python

from cartopy.io.img_tiles import GoogleTiles

class OpenTopoMap(GoogleTiles):
    def _image_url(self, tile):
        x, y, z = tile
        url = 'https://c.tile.opentopomap.org/%s/%s/%s.png' % (
            z, x, y)
        print(url)
        return url
