#!/usr/bin/env python

import matplotlib.pyplot as pyplot
from visual.mapper.opentopomapTiles import OpenTopoMap
import cartopy.crs as crs
#from cartopy.io.img_tiles import StamenTerrain

class Mapper():
    def __init__(self, bounds):
        fig = pyplot.figure(figsize=(20,20))
        fig.tight_layout()
        tiler = OpenTopoMap()
        #tiler = StamenTerrain()
        prjctn = tiler.crs
        self.axes = pyplot.axes(projection=prjctn)
        self.axes.set_extent(bounds)
        zoom = 15
        self.axes.add_image(tiler, zoom)

    def addTrack(self, lons, lats):
        pyplot.plot(lats, lons, 'm-', transform=crs.Geodetic(), linewidth=5)

    def plot(self, imgPath):
        pyplot.tight_layout()
        pyplot.savefig(imgPath)
        pyplot.clf()
        pyplot.cla()
        pyplot.close()
