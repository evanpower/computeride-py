#!/usr/bin/env python

class MarkdownWriter():
    def __init__(self, path):
        self.mdFile = open(path, 'w')

    def writeMetadata(self, title=None, date=None, modified=None, category=None,
                      tags=None, slug=None, authors=None, summary=None):
        if title:
            self.mdFile.write('Title: {}\n'.format(title))

        if date:
            self.mdFile.write('Date: {}\n'.format(date))

        if modified:
            self.mdFile.write('Category: {}\n'.format(category))

        if tags:
            self.mdFile.write('Tags: {}\n'.format(tags))

        if slug:
            self.mdFile.slug('Slug: {}\n'.format(slug))

        if authors:
            self.mdFile.write('Authors: {}\n'.format(authors))

        if summary:
            self.mdFile.write('Summary: {}\n'.format(summary))


    def writeHeader(self, headerText, level=1):
        headerString = ''
        for i in range(0,level):
            headerString = headerString + '#'
        headerString = headerString + ' ' + headerText + '\n' 

        self.mdFile.write(headerString)

    def writeContent(self, contentText):
        self.mdFile.write(contentText + '  \n')

    def insertImage(self, imagePath):
        self.mdFile.write('![]({})\n'.format(imagePath))

    def close(self):
        self.mdFile.close()
