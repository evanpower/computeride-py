#!/usr/bin/env python

import os.path
import gpxpy
import matplotlib.dates as dates
from visual.mapper.mapper import Mapper
from visual.elevation.elevation import PlotElevation
from markdownWriter.markdownWriter import MarkdownWriter
import math
import array
from statsCollector import StatsCollector


def format_time(time_s):
    if not time_s:
        return 'n/a'
    else:
        minutes = math.floor(time_s / 60.)
        hours = math.floor(minutes / 60.)
    return '%s:%s:%s' % (str(int(hours)).zfill(2), 
                         str(int(minutes % 60)).zfill(2), 
                         str(int(time_s % 60)).zfill(2))


def post(trackFile, makePost=True):
    
    trackFile = "./data/rides/6AQI5619.gpx"

    track = gpxpy.parse(open(trackFile))

    length_3d = track.length_3d()

    moving_time, stopped_time, moving_distance, stopped_distance, max_speed = track.get_moving_data()
    uphill, downhill = track.get_uphill_downhill()

    trackPoints = track.walk()

    stats = StatsCollector()

    for p in trackPoints:
        stats.addPoint(p)

    outputDir = 'output/ride-{}/'.format(stats.date.date())
    imgDir = '{}/img/'.format(outputDir)
    try:
        os.stat(outputDir)
    except:
        os.mkdir(outputDir)
    try:
        os.stat(imgDir)
    except:
        os.mkdir(imgDir)
    outMdFile = outputDir + 'ride.md'
    outElvFile = imgDir + 'elevation.png'
    outMapFile = imgDir + 'map.png'

    elvBounds = [stats.ptDist[0], stats.ptDist[-1], stats.minElv*0.9, stats.maxElv*1.1]
    PlotElevation(stats.ptDist, stats.ptElv, elvBounds, outElvFile)
    lonMargin = (stats.maxLon-stats.minLon) * 0.1
    latMargin = (stats.maxLat-stats.minLat) * 0.1
    mapBounds = (stats.minLon - lonMargin, 
                 stats.maxLon + lonMargin, stats.minLat - latMargin,
                 stats.maxLat + latMargin)
    print(mapBounds)
    m = Mapper(mapBounds)
    m.addTrack(stats.ptLat, stats.ptLon)
    m.plot(outMapFile)

    wr = MarkdownWriter(outMdFile)
    wr.writeMetadata(title='{} Ride'.format(str(stats.date.date())),
                     date=str(stats.date.date()),
                     category='Bike Rides')
    wr.writeHeader(str(stats.date.date()), 1)
    wr.writeContent('Today I went for a bike ride.')
    wr.writeContent('Distance {:.1f}km'.format(length_3d/1000))
    wr.writeContent('Moving Time: ' + format_time(moving_time))
    wr.writeContent('Chilling time: ' + format_time(stopped_time))
    wr.writeContent('Elevation gain: {:.0f}m'.format(uphill))
    wr.writeContent('Average speed: {:.2}km/h'.format((length_3d/1000) / 
                                                      (moving_time/3600)))
    wr.writeContent('Max speed: {:.2f}km/h'.format(max_speed * 3.6))

    wr.insertImage(os.path.relpath(outElvFile, start=outputDir))
    wr.writeContent('')
    wr.insertImage(os.path.relpath(outMapFile, start=outputDir))
    wr.close()

