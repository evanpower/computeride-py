#!/usr/bin/env python

import sqlite3

def main():
    makeRideDb('test.sqlite')

class column():
    name = None
    dataType = None

    def __init__(self, colName, colType):
        self.name = colName
        self.dataType = colType

    def defStr(self):
        return '{} {}'.format(self.name, self.dataType)


def makeRideDb(filename):
    rideTableName = 'rideTable'
    dateTimeCol = column('dateTime', 'INTEGER')
    distCol = column('distance', 'REAL')
    movingTimeCol = column('movingTime', 'INTEGER')
    totalTimeCol = column('totalTime', 'INTEGER')
    elvGainCol = column('elvGain', 'REAL')
    elvLossCol = column('elvLoss', 'REAL')
    bikeCol = column('bike', 'TEXT')

    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('CREATE TABLE {tn} ({c1},\
                                  {c2},\
                                  {c3},\
                                  {c4},\
                                  {c5},\
                                  {c6},\
                                  {c7})'.format(tn=rideTableName,
                                                c1=dateTimeCol.defStr(),
                                                c2=distCol.defStr(),
                                                c3=movingTimeCol.defStr(),
                                                c4=totalTimeCol.defStr(),
                                                c5=elvGainCol.defStr(),
                                                c6=elvLossCol.defStr(),
                                                c7=bikeCol.defStr()))
    conn.commit()
    conn.close()

def addRide(filename, dateTime, distance, movingTime, totalTime, elvGain,
            elvLoss, bike):
    x = None


main()
