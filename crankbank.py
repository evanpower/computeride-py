#!/usr/bin/env python

import argparse
import computeride

def main():
    argParser = argparse.ArgumentParser(
        description='Extract data from a gpx file and maybe make a post about it.',)
    argParser.add_argument('path', type=str, help= 
        'Path to a gpx file to be processed.')
        
    args = argParser.parse_args()
    print(args.path)
    
    computeride.post(args.path)
    
main()